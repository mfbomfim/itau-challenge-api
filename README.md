# ItauChallengeAPI

### Install ###

* After clone project open folder and run `npm i`

### MongoDB ###


I am assuming that you already have MongoDB in your environment or a docker. I will not teach you how to install MongoDB. 
If in doubt consult the official documentation https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/, I'm using Windows, choose the version to your SO.

* Start MongoDB in localhost or in docker
* Open console of mongoDB and run `use itauchallenge` to create DataBase
* After running command above
```
db.createUser({
    "user" : "challenge",
    "pwd": "itau@challenge",
    "roles" : [
        {
            "role" : "readWrite",
            "db" : "itauchallenge"
        }
    ]
});
```
to generate user and password


### Run Project ###

The project can be started in a different way. Below are the 2 options


* If you want to debug run `npm run dev`
* without debug option run `npm start`
