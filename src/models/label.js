'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    maxlength: 12,
  },
  userId: {
    type: String,
    required: true,
    trim: true,
  }
});

module.exports = mongoose.model('Label', user);
