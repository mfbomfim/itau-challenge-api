const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
require('./config');

const app = express();

// const connectDB = await ptBr.connections[process.env.NODE_ENV];

mongoose.connect(/*connectDB*/'mongodb://localhost:27017/itauchallenge', {
  auth: {
    user: 'challenge',
    password: 'itau@challenge'
  },
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})

mongoose.connection.on('error', (err) => {
  console.error(`Mongoose connection error: ${err}`);
  process.exit(1);
});

require('./models/expense');
require('./models/user');
require('./models/label');

const indexRouter = require('./routes/index');
const authRouter = require('./routes/authRouter');
const expenseRouter = require('./routes/expenseRouter');
const labelRouter = require('./routes/labelRouter');
const userRouter = require('./routes/userRouter');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use('/', indexRouter)
app.use('/auth', authRouter);
app.use('/expenses', expenseRouter);
app.use('/label', labelRouter);
app.use('/users', userRouter);

module.exports = app;