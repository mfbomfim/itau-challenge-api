module.exports = {
  connections: {
    development: 'mongodb://localhost:27017/itauchallenge',
    production: 'mongodb://user_here:psw_here@mongodb.link_DB_here/itauchallenge',
  },
  error: {
    email_is_already_in_use_by_another_user: 'Email já está em uso por outro usuário',
    expense_can_not_deleted: 'Despesa não pode ser deletada!',
    expense_can_not_registered: 'Despesa não pode ser cadastrada!',
    expense_can_not_updated: 'Despesa não pode ser atualizada!',
    label_can_not_deleted: 'Rótulo não pode ser deletada!',
    label_can_not_registered: 'Rótulo não pode ser cadastrada!',
    label_can_not_updated: 'Rótulo não pode ser atualizada!',
    user_can_not_registered: 'Usuário não pode ser cadastrado!',
  },
  success: {
    expense_successfully_deleted: 'Despesa deletada com sucesso!',
    expense_successfully_registered: 'Despesa cadastrada com sucesso!',
    expense_successfully_updated: 'Despesa atualizada com sucesso!',
    label_successfully_deleted: 'Rótulo deletada com sucesso!',
    label_successfully_registered: 'Rótulo cadastrada com sucesso!',
    label_successfully_updated: 'Rótulo atualizada com sucesso!',
    user_successfully_registered: 'Usuário cadastrado com sucesso!',
  }
};
