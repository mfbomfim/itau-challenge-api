'use strict';

exports.buildToken = (user) => {
  return {
    id: user._id.toString(),
    name: user.name,
    email: user.email
  };
};

exports.build = (user) => {
  return {
    name: user.name,
    email: user.email
  };
};