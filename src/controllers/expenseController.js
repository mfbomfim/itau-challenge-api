'use strict';

const repository = require('../repositories/expenseRepository');
const registryExpenseValidate = require('../validators/RegistryExpenseValidate');
const ptBr = require('../environment/properties_pt-BR');
const authService = require('../services/authService');

exports.post = async (req, res) => {
  const user = await authService.getUserInToken(req);
  const expense = {
    ...req.body,
    userId: user.id,
  }

  const contract = registryExpenseValidate.check(expense);

  if (!contract.isValid()) {
    res.status(400).send({
      message: contract.errors(),
    });

    return;
  }

  repository.save(expense)
    .then(() => res.status(201).send({
      message: ptBr.success.expense_successfully_registered,
    }))
    .catch(erro => {
      res.status(400).send({
        message: ptBr.error.expense_can_not_registered,
        data: erro,
      });
    });
}

exports.get = async (req, res) => {
  const user = await authService.getUserInToken(req);

  repository.filterBy({ userId: user.id})
    .then(data => res.status(200).send(data))
    .catch(e => res.status(500).send(e));
}

exports.put = (req, res) => {
  const contract = registryExpenseValidate.check(req.body);

  if (!contract.isValid()) {
    res.status(400).send({
      message: contract.errors(),
    });

    return;
  }

  repository
    .update(req.params.id, req.body)
    .then(() => res.send(201).send({
      message: ptBr.success.expense_successfully_updated,
    }))
    .catch(erro => {
      res.status(400).send({
        message: ptBr.error.expense_can_not_updated,
        data: erro,
      });
    });
}

exports.filter = async (req, res) => {
  const user = await authService.getUserInToken(req);
  const filter = {
    ...req.query,
    userId: user.id,
  };
  
  repository.filterBy(filter)
    .then(data => res.status(200).send(data))
    .catch(e => res.status(400).send(e));
}

exports.delete = (req, res,) => {
  repository
    .delete(req.params.id)
    .then(() => res.send(205).send({
      message: ptBr.success.expense_successfully_deleted,
    }))
    .catch(erro => {
      res.status(500).send({
        message: ptBr.error.expense_can_not_deleted,
        data: erro,
      });
    });
}