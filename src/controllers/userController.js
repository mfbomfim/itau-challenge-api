'use strict';

const repository = require('../repositories/userRepository');
const registryUserValidate = require('../validators/RegistryUserValidate');
const userTransform = require('../transform/userTransform');
const ptBr =  require('../environment/properties_pt-BR');

exports.post = async (req, res) => {
  const contract = registryUserValidate.check(req.body);

  if (!contract.isValid()) {
    res.status(400).send({
      message: contract.errors(),
    });

    return;
  }

  try {
    await repository.create(userTransform.build(req.body));
    res.status(201).send({ message: ptBr.success.user_successfully_registered });
  } catch (e) {
    const msg = e.code === 11000 ? ptBr.error.email_is_already_in_use_by_another_user : ptBr.error.user_can_not_registered;

    res.status(400).send({
      message: msg
    });
  }
}

exports.get = (req, res) => {
  repository.find()
    .then(data => res.status(200).send(data))
    .catch(e => res.status(500).send(e));
}