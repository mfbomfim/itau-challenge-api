'use strict';
const repository = require('../repositories/userRepository');
const authValidade = require('../validators/AuthValidate');
const userTransform = require('../transform/userTransform');
const userLoggedTransform = require('../transform/userLoggedTransform');
const authService = require('../services/authService');

exports.post = async (req, res) => {
  const contract = authValidade.check(req.body);

  if (!contract.isValid()) {
    res.status(400).send({
      message: contract.errors(),
    });

    return;
  }

  try {
    const response = await repository.auth(userTransform.buildAuth(req.body));

    if (!response || !response.length) {
      res.status(404).send({
        message: 'Login ou senha inválido!'
      });
      return;
    }

    const token = await authService.generateToken(userLoggedTransform.buildToken(response[0]));

    res.status(201).send({
      token: token,
      user: userLoggedTransform.build(response[0])
    });
  } catch (e) {
    res.status(500).send({
      message: 'Erro interno!'
    });
  }
}

exports.get = async (req, res, next) => {
  res.status(200).send('0k');
}