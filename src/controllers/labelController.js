'use strict';

const repository = require('../repositories/labelRepository');
const registryLabelValidate = require('../validators/RegistryLabelValidate');
const authService = require('../services/authService');
const ptBr =  require('../environment/properties_pt-BR');

exports.post = async (req, res) => {
  const user = await authService.getUserInToken(req);
  const label = {
    ...req.body,
    userId: user.id,
  };

  const contract = registryLabelValidate.check(label);

  if (!contract.isValid()) {
    res.status(400).send({
      message: contract.errors(),
    });

    return;
  }

  repository
    .save(label)
    .then(() => res.status(201).send({
      message: ptBr.success.label_successfully_registered,
    }))
    .catch(erro => {
      res.status(400).send({
        message: ptBr.error.label_can_not_registered,
        data: erro,
      });
    });
}

exports.get = async (req, res) => {
  const user = await authService.getUserInToken(req);

  repository.get({ userId: user.id})
    .then(data => res.status(200).send(data))
    .catch(e => res.status(500).send(e));
}

exports.put = (req, res) => {
  const contract = registryLabelValidate.check(req.body);

  if (!contract.isValid()) {
    res.status(400).send({
      message: contract.errors(),
    });

    return;
  }

  repository
    .update(req.params.id, req.body)
    .then(() => res.send(201).send({
      message: ptBr.success.label_successfully_updated,
    }))
    .catch(erro => {
      res.status(400).send({
        message: ptBr.error.label_can_not_updated,
        data: erro,
      });
    });
}

exports.delete = (req, res,) => {
  repository
    .delete(req.params.id)
    .then(() => res.send(205).send({
      message: ptBr.success.label_successfully_deleted,
    }))
    .catch(erro => {
      res.status(500).send({
        message: ptBr.error.label_can_not_deleted,
        data: erro,
      });
    });
}