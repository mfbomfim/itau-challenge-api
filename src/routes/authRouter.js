'use strict'

const express = require('express');
const controller = require('../controllers/authController');
const router = express.Router();
const authService = require('../services/authService');

router.post('/', controller.post);
router.get('/',authService.authorize, controller.get);

module.exports = router;