'use strict'

const express = require('express');
const controller = require('../controllers/expenseController');
const router = express.Router();
const authService = require('../services/authService');

router.post('/', authService.authorize, controller.post);
router.get('/', authService.authorize, controller.get);
router.get('/filter', authService.authorize, controller.filter);
router.put('/:id/update', authService.authorize, controller.put);
router.delete('/:id/delete', authService.authorize, controller.delete);

module.exports = router;