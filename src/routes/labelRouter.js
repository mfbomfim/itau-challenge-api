'use strict'

const express = require('express');
const controller = require('../controllers/labelController');
const authService = require('../services/authService');
const router = express.Router();

router.post('/', authService.authorize, controller.post);
router.get('/', authService.authorize, controller.get);

module.exports = router;