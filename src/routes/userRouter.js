'use strict'

const express = require('express');
const controller = require('../controllers/userController');
const authService = require('../services/authService');
const router = express.Router();

router.post('/', controller.post);
router.get('/', authService.authorize, controller.get);

module.exports = router;