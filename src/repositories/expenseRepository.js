const mongoose = require('mongoose');
const Expense = mongoose.model('Expense');

exports.get = () => {
  return Expense.find({});
}

exports.save = (data) => {
  const expense = new Expense(data);
  return expense.save();
}

exports.update = (id, expense) => {
  return Expense.findByIdAndUpdate(id, {
    $set: {
      ...expense
    }
  })
}

exports.delete = (id) => {
  return Expense.findByIdAndDelete(id);
}

exports.filterBy = (filter) => {
  return Expense.find({ ...filter }, 'date name group expenseAmount');
}