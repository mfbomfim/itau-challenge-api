const mongoose = require('mongoose');
const User = mongoose.model('User');

exports.create = (data) => {
  const user = new User(data);
  return user.save();
}

exports.auth = async (data) => {
  const user = await User.find({
    email: data.email,
    password: data.password,
  }, 'name email');

  return user;
}

exports.find = async () => {
  const user = await User.find({}, 'name email');

  return user;
}