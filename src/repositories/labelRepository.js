const mongoose = require('mongoose');
const Label = mongoose.model('Label');

exports.save = (data) => {
  const label = new Label(data);
  return label.save();
}

exports.get = async (filter) => {
  const label = await Label.find({ ...filter }, 'name');

  return label;
}

exports.update = (id, label) => {
  return Label.findByIdAndUpdate(id, {
    $set: {
      ...label
    }
  })
}

exports.delete = (id) => {
  return Label.findByIdAndDelete(id);
}