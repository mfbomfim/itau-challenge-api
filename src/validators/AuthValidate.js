'use strict';
const ValidatorContract = require('./fluent-validator');

exports.check = (user) => {
  const contract = new ValidatorContract();

  contract.isRequired(user.email, 'O login é obrigatório.');
  contract.isEmail(user.email, 32, 'Login inválido');

  contract.isRequired(user.password, 'A senha é obrigatório.');
  contract.hasMaxLen(user.password, 32, 'A senha deve ter no máximo 32 caracteres.');
  contract.hasMinLen(user.password, 5, 'A senha deve ter no mínimo 5 caracteres.');

  return contract;
}