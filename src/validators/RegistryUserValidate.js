'use strict';
const ValidatorContract = require('./fluent-validator');

exports.check = (user) => {
  const contract = new ValidatorContract();

  contract.isRequired(user.name, 'O nome é obrigatório.');
  contract.hasMaxLen(user.name, 128, 'O nome deve ter no máximo 128 caracteres.');
  contract.hasMinLen(user.name, 3, 'O nome deve ter no mínimo 3 caracteres.');

  contract.isRequired(user.password, 'A senha é obrigatório.');
  contract.hasMinLen(user.password, 8, 'A senha deve ter no mínimo 9 caracteres.');

  contract.isRequired(user.email, 'O E-mail é obrigatório.');
  contract.isEmail(user.email, 'E-mail inválido');

  return contract;
};