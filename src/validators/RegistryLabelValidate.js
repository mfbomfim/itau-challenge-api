'use strict';

const ValidatorContract = require('./fluent-validator');

exports.check = (label) => {
  const contract = new ValidatorContract();

  contract.isRequired(label.name, 'O nome é obrigatório.');
  contract.hasSpecialCharacters(label.name, 'API não aceita caracteres especiais para esse endpoint');

  return contract;
};
