'use strict';
const ValidatorContract = require('./fluent-validator');

exports.check = (expense) => {
  const contract = new ValidatorContract();

  contract.isRequired(expense.date, 'A data é obrigatória.');
  contract.isRequired(expense.name, 'O nome é obrigatório.');
  contract.isRequired(expense.expenseAmount, 'Valor da despesa é obrigatório');
  contract.isRequired(expense.group, 'O grupo é obrigatório.');

  return contract;
};
