const app = require('../src/app');
const http = require('http');
const debug = require('debug')('nodestr:server');
const dotenv = require('dotenv');

dotenv.config();


const port = normalizePort(process.env.PORT);

app.set('port', port);

const server = http.createServer(app);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
  if (process.env.NODE_ENV !== 'development') {
    console.log('App rodando na porta 8080');
    return 8080;
  } else {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
      return val;
    }

    if (port >= 0) {
      return port;
    }

    console.log('App rodando na porta ', val);

    return false;
  }
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  const address = server.address();
  const bind = typeof address === 'string' ? `Pipe ${address}` : `Port ${address.port}`;

  console.log('Server started on port:', port);

  debug(`Listening on ${bind}`);
}